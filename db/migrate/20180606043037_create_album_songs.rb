class CreateAlbumSongs < ActiveRecord::Migration[5.1]
  def change
    create_table :album_songs do |t|
      t.references :album, foreign_key: true
      t.references :song, foreign_key: true
      t.text :description
      t.integer :position

      t.timestamps
    end
  end
end
