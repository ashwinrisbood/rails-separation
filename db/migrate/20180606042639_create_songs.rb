class CreateSongs < ActiveRecord::Migration[5.1]
  def change
    create_table :songs do |t|
      t.string :name
      t.text :artist
      t.string :genre
      t.text :description

      t.timestamps
    end
  end
end
