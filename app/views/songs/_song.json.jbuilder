json.extract! song, :id, :name, :artist, :genre, :description, :created_at, :updated_at
json.url song_url(song, format: :json)
