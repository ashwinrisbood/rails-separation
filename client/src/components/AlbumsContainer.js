import React, { Component } from 'react' ;
import axios from 'axios';

class AlbumsContainer extends Component{
    constructor(props){
        super(props)
        this.state = {
            Albums: []
        }
    }

    componentDidMount() {
        axios.get('api/v1/albums.json')
        .then(response => {
            console.log(response)
            this.setState({
                Albums: response.data
            })
        })
        .catch(error => console.log(error))
    }
  render() {
    return (
      <div className="Albums-container">
          Albums
          {this.state.Albums.map( Album => {
            return (
              <div className="ls" key={Album.id}>
                  <p>{Album.name}</p>
                  <p>{Album.artist}</p>
              </div>
            )
          })}
      </div>
    )
  }
}

export default AlbumsContainer;
