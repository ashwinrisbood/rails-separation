Rails.application.routes.draw do
  resources :songs
  resources :albums
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
      namespace :v1 do
        resources :songs
        resources :albums
      end
    end
end
